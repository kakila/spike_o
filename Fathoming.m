function fathom      =   Fathoming(Tree,Alive,Tol,multicrit)  

% -------------------------------------------------------------------------
% SPIKE_O toolbox - Fathoming.m
%
% This function evaluates which nodes are to be fathomed as part of the
% branch-and-bound algorithm
%
% Syntax:
%   fathom  =   Fathoming(Tree,Alive,Tol,multicrit)
%
% Inputs:
%   Tree        :   Solution tree
%   Alive       :   Boolean vector indicating whether nodes are alive or
%                   not 
%   Tol         :   [optional] Scalar indicating tolerance to be used
%                   during fathoming procedure (default = 1e-9) 
%   multicrit   :   [optional] number of competing objectives (default = 1) 
%
% Outputs:
%   fathom      :   List of nodes to be fathomed
%
% -------------------------------------------------------------------------
% Latest version: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------

if nargin<=2 || isempty(Tol)
    Tol = 1e-9 ;
end

% column indices for lower bound and upper bound:
if nargin<4 || isempty(multicrit)
    multicrit = 1;
end
Indices

nAlive          =   length(Alive)       ;   % number of live nodes

nTree           =   size(Tree,1)        ;
AliveBool       =   false(nTree,1)      ;
AliveBool(Alive)=   true                ;

fathom          =   zeros(nAlive,1)     ;   % set fathom to 0/false (unmarked)


for j = 1:nAlive    % for each live node
    lower       =   Tree(Alive(j),iLB)  ;   % lower bound corresponding to current node
    
    % if there is any other node which has an upper bound lower than this
    % lower bound, then mark this live node to be fathomed (1/true)
    OthersAliveBool  =  AliveBool;
    OthersAliveBool(Alive(j)) = false ;

    if any(OthersAliveBool)
        if multicrit==1
            fathom(j)   =   min(Tree(OthersAliveBool,iUB))+Tol<lower   ;
        else
            if any(OthersAliveBool)
                AliveNum = find(OthersAliveBool) ;
                UBs =  Tree(AliveNum,iUB) ;
                LBs = repmat(lower,[length(AliveNum) 1]) ;
                if any(all(UBs<LBs,2))
                    % there exists a solution for which all UBs are lower
                    % and the considered solution's LBs
                    fathom(j)=true ;
                else
                    % there exists a solution for which:
                    % 1. all UBs are equal or lower than the considered solution's LBs
                    % 2. at least one UB is stricly lower than the considered solution's LB
                    if any(all([all(UBs<=LBs,2),any(UBs<LBs,2)],2))
                        fathom(j) = true;
                    end
                    %             for crit=1:multicrit
                    %                 crit
                    %
                    %                 count = count + 1*any(Tree(find(OthersAliveBool),iUB(crit))<lower(crit));
                    %             end
                    %             if count==multicrit
                end
            end
        end
    end
end
if all(fathom)
    Tree(Alive,iLB)
    Tree(Alive,iUB)
end
fathom          =   find(fathom)        ;   % list nodes to be fathomed
fathom          =   unique(fathom)      ;   % list as unique nodes

% -------------------------------------------------------------------------
function Fate = Reckoning1(Tree,OthersAlive,iUB,lower,nOA)

UB          =   Tree(OthersAlive,iUB)                   ;
RefLB       =	repmat(lower,[nOA 1])                   ;
c1          =   all(UB<=RefLB,2)                        ;
c2          =   sum(UB<RefLB,2)>=1                      ;
Fate        =   any(and(c1,c2))                         ;

% -------------------------------------------------------------------------
function Fate = Reckoning2(Tree,OthersAlive,iUB,lower,nOA,multicrit)

c1 = zeros(nOA,1) ;
c2 = zeros(nOA,1) ;

for crit=1:multicrit
    ub  =   Tree(OthersAlive,iUB(crit)) ;
    ref =   lower(crit)                 ;
    c1  =   c1 + 1*(ub<=ref)                ;
    c2	=   c2 + 1*(ub<ref)                 ;
end

c1          =   c1==multicrit   ;
c2          =   c2>0            ;
Fate        =   any(and(c1,c2)) ;       

return