SPIKE_O toolbox v1.2 - Spike Optimization toolbox
by Kris Villez

Website:	http://homepages.eawag.ch/~villezkr/spike/
Contact: 	kris.villez@eawag.ch

This toolbox is a collection of Matlab/Octave optimization functions and scripts originally written for the Generalized Shape Constrained Spline fitting method but that are also applicable elsewhere. They are released to the public under the GPL v3 license in order to encourage the sharing of code and prevent duplication of effort. If you find these functions useful, drop me a line. I also appreciate bug reports and suggestions for improvements. For more detail regarding copyrights see the file LICENSE.TXT located in the same folder where this README file is located.

These files are provided as is, with no guarantees and are intended for non-commercial use. These routines have been developed and tested successfully with Matlab (R2010a) on a Windows system. Tests in Octave are incomplete at the time of writing. To install the Matlab functions in a Windows systems, using Matlab or Octave, follow the instructions below.

---------------------------
 I. Installing the toolbox
---------------------------

1. Create a directory to contain the Matlab functions.  For example, in my case the directory is

		C:\Tools\SPIKE_O

2. Unzip the contents of the file "SPIKE_O.zip" into this directory.



-----------------------
 II. Using the toolbox
-----------------------

1. Start Matlab

2. To enable access to the functions in the SPIKE_O toolbox from any other folder, add the toolbox to the Matlab/Octave path by typing the following in the command line

		addpath C:\Tools\SPIKE_O

This allows access during the current Matlab/Octave session.

3. To enable access to the SPIKE_O toolbox permanently (across sessions), type the following in the command line:
	
		savepath

4. For help with the SPIKE_O toolbox, type 'help FUN' (without quotes) and FUN the actual function you want help with, e.g.:
	
		help BB

5. A demo of the Branch-and-Bound algorithm is provided in the SPIKE_O toolbox in folder:
	
		C:\Tools\SPIKE_O\DemoBB
	
	To run it, ensure you change your working directory to the above directory and run the following script:
	
		DemoBB.m
	


----------------------------
 III. Relevant publications
----------------------------

Please refer to this toolbox by citing following publication:
[1] Villez, K., Venkatasubramanian, V., Rengaswamy, R. (2013). Generalized Shape Constrained Spline Fitting for Qualitative Analysis of Trends. Computers & Chemical Engineering, 58, 116-134.

Other relevant SPIKE publications are the following:
[2] Villez, K.; Rieger, L.; Keser, B. ; Venkatasubramanian, V. (2012). Probabilistic qualitative analysis for fault detection and identification of an on-line phosphate analyzer. International Journal of Advances in Engineering Sciences and Applied Mathematics, 4, 67-77.
[3] Villez, K.; Ros�n, C.; Anctil, F.; Duchesne, C.; Vanrolleghem, P.A. (2013). Qualitative Representation of Trends (QRT): Extended method for identification of consecutive inflection points. Computers and Chemical Engineering, 48, 187-199.


-------------
 IV. Changes
-------------

- OptimsetBB.m was modified to support the corresponding changes in definition of knot vectors (in cell array) in the SCS toolbox v2.0.
- Toolbox functions related to the BB algorithm are modified to support multicriteria optimization

------------------
 Acknowledgements
------------------

I want to thank the following people for testing the toolbox and their subsequent suggestions:
- Christian M. Th�rlimann
- Jonathan M. Habermacher

----------------------------------------------------------
Last modified on 21 November 2013