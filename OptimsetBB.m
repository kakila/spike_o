function options = OptimsetBB(model)

% -------------------------------------------------------------------------
% SPIKE_O toolbox - OptimsetBB.m
%
% This function generates default options for the branch-and-bound
% algorithm
%
% Syntax:
%   options = OptimsetBB(model)
%
% Inputs:
%   model   :   mathematical problem
%
% Outputs:
%   options :   structure with all default options
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------



type='unknown';
if nargin>=1 && ~isempty(model)
    if isfield(model,'type')
        type =model.type;
    elseif ischar(model)
        type =model ;
    end
end

switch type
    case 'SCSD'
        
        default.boundfun    =   @BoundSCS      ;
        default.boundlow    =   @BoundSCSlow    ;
        default.boundupp    =   @BoundSCSupp    ;
        default.earlystop   =   0               ;
        % Node selection:
            %   1=breadth first
            %   2=best upper bound
            %   3=best lower bound 
            %   4=depth first
        default.node        =   1               ;
        % Variable selection:
            %   1=largest range, only currently supported option
        default.var         =   1               ;
        default.rounding    =   1               ;
        % Branching strategy
            %   1=split along 1 selected variable
            %   2=split along all variables
            %   3=split all variables at the same splitting value. this is
            %   only valid if all variables are subject to monotonicity
            %   constraints, e.g. x(j)>=x(j-1) for all j>=2.
        default.branching   =   3               ;
        % Splitting = Number of branches per variable. Only applied if
        % branching is 1 or 2. Total number of branches = (#splitted
        % variables)^splitting.
        default.splitting   =   2               ;
        default.resolution  =   1               ;
        if isfield(model,'QR')
            if any(model.QR(:,end)>0)
                default.resolution	=	(1/2)^5	; 
            end
        end
        default.maxiter     =   1e6             ;
        default.verbose     =   2               ; % values: 0-2
        default.verify      =   1               ; % values
        default.graph       =   1               ;
        
        if isfield(model,'tk')
            if isfield(model,'multivar')
                defaultsplit        =   [ nan ; nan ];
                for j=1:model.multivar
                    defaultsplit(1)     = min(defaultsplit(1),model.tk{j}(1)) ;
                    defaultsplit(2)     = min(defaultsplit(2),model.tk{j}(end)) ;
                end
                default.defaultsplit   =   defaultsplit             ;
            else
                default.defaultsplit   =   model.tk([1 end])             ;
            end
        else
            default.defaultsplit   =   [-inf inf]               ;
        end
        
        options             =   default     ;
        
    case 'SCS'
        default.boundfun    =   @BoundsSCS      ;
        default.boundlow    =   @BoundlowSCS    ;
        default.boundupp    =   @BounduppSCS    ;
        default.earlystop   =   0               ;
        % Node selection:
            %   1=breadth first
            %   2=best lower bound 
            %   3=best upper bound 
            %   4=depth first
        default.node        =   1               ;
        % Variable selection:
            %   1=largest range, only currently supported option
        default.var         =   1               ;
        default.rounding    =   1               ;
        % Branching strategy
            %   1=split along 1 selected variable
            %   2=split along all variables
            %   3=split all variables at the same splitting value. this is
            %   only valid if all variables are subject to monotonicity
            %   constraints, e.g. x(j)>=x(j-1) for all j>=2.
        default.branching   =   3               ;
        % Splitting = Number of branches per variable. Only applied if
        % branching is 1 or 2. Total number of branches = (#splitted
        % variables)^splitting.
        default.splitting   =   2               ;
        default.resolution  =   1               ;
        default.maxiter     =   1e6             ;
        default.verbose     =   2               ; % values: 0-2
        default.verify      =   0               ; % values
        default.graph       =   1               ;
        if isfield(model,'tk')
            if isfield(model,'multivar')
                defaultsplit        =   [ nan ; nan ];
                for j=1:model.multivar
                    defaultsplit(1)     = min(defaultsplit(1),model.tk{j}(1)) ;
                    defaultsplit(2)     = min(defaultsplit(2),model.tk{j}(end)) ;
                end
                default.defaultsplit   =   defaultsplit             ;
            else
                default.defaultsplit   =   model.tk([1 end])             ;
            end
        else
            default.defaultsplit   =   [-inf inf]               ;
        end
        options             =   default     ;
    otherwise
%        default.boundfun    	=   @Bounds         ;
%        default.boundlow    	=   @Boundlow       ;
%        default.boundupp    	=   @Boundupp       ;
        default.earlystop   	=   0               ;
        default.node        	=   1               ;
        default.var         	=   1               ;
        default.rounding    	=   0               ;
        default.branching   	=   1               ;
        default.splitting   	=   2               ;
        default.resolution  	=   1e-2            ;
        default.maxiter     	=   1e6             ;
        default.verbose     	=   2               ; % values: 0-3
        default.verify      	=   0               ; % values
        default.graph       	=   1               ;
        default.defaultsplit   	=   []              ;
        options             	=   default     	;
end