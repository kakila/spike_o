% SPIKE_O toolbox v1.3
%
% Files
%   BB          	- Branch-and-Bound (B&B) algorithm
%   ContractBound   - Bound contraction algorithm
%   DynProg        	- Dynamic Programming (DP) algorithm
%   Fathoming   	- Fathoming step for B&B
%   GenerateLeafs	- Branching step for B&B
%   Indices         - Generated index variables for B&B
%   OptimsetBB      - Default options for B&B algorithm
%   Ranger          - Alternative for Matlab's range function
%   SelectFromTree  - Returns best solution from the solution tree
%   VisualLeaf      - Vizualization aid - plot leaf in the solution tree
%   VisualTree     	- Vizualization aid - plot branches/leaves in the solution tree
%   VisualTreeNodes	- Vizualization aid - plot solution tree iteratively

