function Leafs = GenerateLeafs(Tree,branch, index,nib,vv,Verbose,options,DeltaX,model)

% -------------------------------------------------------------------------
% SPIKE_O toolbox - GenerateLeafs.m
%
% This function generates new sets from an existing one (branching)
%
% Syntax:
%   Leafs = GenerateLeafs(Tree,branch,index,nib,vv,Verbose,options,DeltaX,model)
%
% Inputs:
%   Tree    :   Solution tree
%   branch  :   leaf to be branched from
%   index   :   column index for start of set description
%   nib     :   number of variables
%   vv      :   index of variables which can be branched
%   Verbose :   verbose output or not
%   options :   optimization options
%   DeltaX  :   requested variable resolution
%   model   :   mathematical problem
%
% Outputs:
%   Leafs   :   New nodes
%
% -------------------------------------------------------------------------
% Last modification: Kris Villez, 2016-01-27
% -------------------------------------------------------------------------

% -------------------------------------------------------------------------
% Copyright 2012-2016 Kris Villez
%
% This file is part of the Spike_O Toolbox for Matlab/Octave. 
% 
% The Spike_O Toolbox is free software: you can redistribute it and/or
% modify it under the terms of the GNU General Public License as published
% by the Free Software Foundation, either version 3 of the License, or (at
% your option) any later version.
% 
% The Spike_O Toolbox is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General
% Public License for more details.
% 
% You should have received a copy of the GNU General Public License along
% with the SPIKE_O Toolbox. If not, see <http://www.gnu.org/licenses/>.  
% -------------------------------------------------------------------------


rounding    =   options.rounding    ;
branching   =   options.branching   ;
splitting   =   options.splitting   ;

Xbranch     =   Tree(branch,index:end)      ;   % intervals as vector
Xbranch     =   reshape(Xbranch,[2 nib])    ;   % reshape into matrix
if isfield(model,'integer')
    integer             =   model.integer               ;
    Xbranch(1,integer)  =   ceil(Xbranch(1,integer))    ;
    Xbranch(2,integer)  =   floor(Xbranch(2,integer))   ;
end

KR          =   Ranger(Xbranch)./DeltaX      ;   % knot-based range of variables in selected subset
inc         =   [   ]   ;
if nargin>=9 && ~isempty(model)
    if isfield(model,'verification')
        [oo,ListVerified]   =   model.verification(model,Xbranch)   ;
        inc                 =   find( ListVerified==0 )             ;
	elseif isfield(model,'type') && strcmp(model.type,'SCSD')
		ExDisc 		=	true ;
		AtResolution 		=	CheckResolution(model,Xbranch',ExDisc) 	;
		inc                 =   find( AtResolution==false )             ;
        
    end
end
if isempty(inc)
    inc =   1:length(vv);
end

%TakeFirst = true;
%if TakeFirst
%    split=find(KR(vv(inc))>1,1,'first');
%    split=vv(inc(split)) ;
%else
    KRmax  		=   max(KR(vv(inc)))                 	;   %   find maximal knot range
	split 		= 	find(KRmax==KR(vv(inc)),1,'first')	;	%   index of first variable with maximal knot range among those considered 
    split       =   vv(inc(split))                   	;	% 	index of first variable with maximal knot range among all
%end

if branching==2
    split = find(KR==KR(split));
end

% define bounds for the splitted variable for the newly generated
% subsets

if splitting==2 
    
    division = [];
    if nargin>=9 && ~isempty(model)
        if isfield(options,'defaultsplit')
            % In some cases, the problem solution or its computed bounds change
            % abruptly at particular values for the considered set. Following
            % part allows such problem-based split to be proposed. Output
            % should be empty if no such bound is available.
            defaultsplit    =	options.defaultsplit(:)';
            inc             =	find(and(defaultsplit>Xbranch(1,split),defaultsplit<Xbranch(2,split)));
            if ~isempty(inc)
                division    =	defaultsplit(inc)*ones(1,length(split)) ;
            end
        end
    end
    if isempty(division)
        
        if rounding
            % Rounding means that solutions are rounded to a power of 2. This
            % is useful if the computed bounds are known to collapse onto
            % themselves when the given resolution is obtained.
            division        =   RoundedDivision(DeltaX,KR,Xbranch,split,splitting)   ;   % rounded divisions
        else
            division        =   DefaultDivision(DeltaX,KR,Xbranch,split,splitting)   ;   % default divisions
        end
        
    end
else
    division = DefaultDivision(DeltaX,KR,Xbranch,split,splitting)   ;   % default divisions
end


switch branching
    case 1 
        % Split along one variable dimension. The number of leaves is
        % fixed.
        limits      =   [   Xbranch(1,split); division; Xbranch(2,split) ]                          ;   % subset bounds

        n_leaf =   splitting           ;
        Leafs  =   cell(n_leaf,1)     ;
        for i_leaf=1:n_leaf
            if Verbose>=3, disp(['  Generating leaf - ' num2str(i_leaf) ' of ' num2str(n_leaf) ]), end                  % display
            Xleaf           =   Xbranch                 ;   % copy branch intervals for leaf
            Xleaf(:,split)  =   limits(i_leaf+(0:1))    ;   % replace proper interval bound with split location
            if isfield(model,'integer')
                integer             =   model.integer               ;
                Xleaf(1,integer)  =   ceil(Xleaf(1,integer))    ;
                Xleaf(2,integer)  =   floor(Xleaf(2,integer))   ;
            end
            Leafs{i_leaf}   =   Xleaf                   ;
        end
    case 2
        % Split along all variable dimensions. The number of leaves depends
        % on the number of variables. 
        limits              =   [   Xbranch(1,split); division; Xbranch(2,split) ]                          ;   % subset bounds

        n_leaf              =       splitting.^(length(split))          ;
        n_leaf = n_leaf(end) ;
        Leafs  =   cell(n_leaf,1)     ;
        Inc = ones(n_leaf,1) ;
        
        n                   =       length(split)                         ;
        IndexStr            =       dec2base((1:n_leaf)-1,splitting,n)  ;
        IndexNum = nan(n_leaf,n);
        for j=1:n
            % do not change to 'str2double'. 
            IndexNum(:,j)   =       str2num(IndexStr(:,j))                     ;
        end
        IndexNum=IndexNum+1;
        for i_leaf  = 1:n_leaf                      ;
            if Verbose>=3, disp(['  Generating leaf - ' num2str(i_leaf) ' of ' num2str(n_leaf) ]), end                  % display
            Xleaf           =   Xbranch             ;
            linInds = sub2ind(size(limits),IndexNum(i_leaf,:),1:n);
            Xleaf(:,split) = [  limits(linInds);  limits(linInds+1) ];
            if isfield(model,'integer')
                integer             =   model.integer               ;
                Xleaf(1,integer)  =   ceil(Xleaf(1,integer))    ;
                Xleaf(2,integer)  =   floor(Xleaf(2,integer))   ;
            end
            Leafs{i_leaf}   =   Xleaf               ;
            Inc(i_leaf) = ~or(any(Xleaf(1,:)>=Xbranch(2,:)),any(Xleaf(2,:)<=Xbranch(1,:)));
            
%             Xleaf(2,split) =
        end
        Leafs = Leafs(find(Inc)) ;
    case 3 
        % split all variables at the same value. This is fruitful if an
        % order exists between the variables such that x(i+1)>=x(i) as a
        % feasibility constraint on the solution. The number of leaves is
        % not fixed a priori. It depends on the number of variables and the
        % actual bounds on the considered set. 
        assert(length(division)==1,'splitting option must be 2 when using value 3 for branching method')
        cc          =   find(and(Xbranch(1,:)<=division,Xbranch(2,:)>=division))  ;
        n_leaf     =   length(cc)+1        ;
        Leafs  =   cell(n_leaf,1)     ;
        for i_leaf=1:n_leaf
            if Verbose>=3, disp(['  Generating leaf - ' num2str(i_leaf) ' of ' num2str(n_leaf) ]), end                  % display
            Xleaf           =   Xbranch             ;
            cla             =   (2:n_leaf)>i_leaf   ;
            c2              =   cc(find(cla))       ;
            c1              =   setdiff(cc,c2)      ;
            
            Xleaf(2,c1)     =   division            ;
            Xleaf(1,c2)     =   division            ;
            if isfield(model,'integer')
                integer             =   model.integer               ;
                Xleaf(1,integer)  =   ceil(Xleaf(1,integer))    ;
                Xleaf(2,integer)  =   floor(Xleaf(2,integer))   ;
            end
            Leafs{i_leaf}   =   Xleaf               ;
        end
end

% -------------------------------------
function division = DefaultDivision(DeltaX,KR,Xbranch,split,splitting)

M1          =   repmat(Xbranch(1,split),[splitting-1,1])                ;
M2          =   (1:splitting-1)'*DeltaX(split).*KR(:,split)/splitting	;
division    =   M1+M2    ;   % default divisions


% -------------------------------------
function div = RoundedDivision(DeltaX,KR,Xbranch,split,splitting)

factor	=   splitting.^(ceil(log(KR(:,split))/log(splitting))-1)    ;
div  	=   repmat(Xbranch(1,split),[splitting-1,1]) ...
                +(1:splitting-1)'*DeltaX(split).*factor             ;
            
diff   	=   Xbranch(2,split)-div	;

if diff<(splitting*1e-8) % use some tolerance to account for computational inaccuracies
    factor	=   splitting.^(ceil(log(KR(:,split))/log(splitting))-2)	;
    div     =   repmat(Xbranch(1,split),[splitting-1,1]) ...
                    +(1:splitting-1)'*DeltaX(split).*factor             ;
end